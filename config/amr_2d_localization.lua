-- Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
-- SPDX-License-Identifier: BSD-3-Clause-Clear 

include "amr_2d.lua"


TRAJECTORY_BUILDER.pure_localization = true
POSE_GRAPH.optimize_every_n_nodes = 50

return options