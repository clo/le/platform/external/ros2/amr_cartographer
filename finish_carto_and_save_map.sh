# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

#!/bin/bash

ws_dir=$(dirname $(dirname $(pwd)))
echo "Workspace DIR: $ws_dir"

source $ws_dir/install/setup.sh

map_dir="$ws_dir/install/amr_cartographer/share/amr_cartographer/map"
map_name="demo_map"
echo "Map DIR: $map_dir"

if [ ! -d "$map_dir" ];then
  echo "Folder does not exist, creating folder ...."
  mkdir -p $map_dir
fi

ros2 service call /qti_save_map cartographer_ros_msgs/srv/WriteState "{filename: '$map_dir/$map_name.pbstream'}"


